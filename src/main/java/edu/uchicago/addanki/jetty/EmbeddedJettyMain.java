package edu.uchicago.addanki.jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 * Embedded Jetty example
 * Created by Kaushal on 8/10/2016.
 */
public class EmbeddedJettyMain {

    public static void main(String[] args) throws Exception {

        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler(server, "/example");
        handler.addServlet(ExampleServlet.class, "/");
        server.start();

    }
}
